
nginx-log-peeker
================

Tool to peek at the NGINX access logs only when needed, without saving
them to disk.

NGINX can send its access logs to a named pipe (FIFO) on the
filesystem, and it does a reasonably good job at fault isolation if
the pipe can't be written to during normal operation. Unfortunately,
it will still block on reloads and other occasions unless there is an
active listener on the pipe. The purpose of this program is to
constantly read from the logs FIFO, and provide a copy of it to
clients connecting to the daemon through a local UNIX socket.

This allows real-time debugging and analysis, without necessarily
having to store logs to disk at any time (which may be undesirable for
compliance purposes).


## Usage

1. Create the FIFO used to communicate with NGINX, and make sure that
   the user that NGINX is running as can write to it:

        $ mkfifo --mode=0700 /var/run/nginx/log-fifo
        $ chown www-data /var/run/nginx/log-fifo

2. Edit your `nginx.conf` to tell NGINX to write the HTTP access logs
   to the named pipe:

        http {
            access_log /var/run/nginx/log-fifo
            ...
        }

3. Start the nginx-log-peeker daemon:

        $ nginx-log-peeker &

4. Connect to the log peeker UNIX socket to retrieve logs in real-time
   whenever you need to debug something:

        $ socat UNIX-CONNECT:/var/run/nginx/log-peek -

