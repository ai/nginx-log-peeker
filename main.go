package main

import (
	"flag"
	"io"
	"log"
	"net"
	"os"
	"sync"
)

var (
	logFifoPath = flag.String("log-fifo", "/var/run/nginx/log-fifo", "location of the NGINX logs FIFO")
	socketPath  = flag.String("socket", "/var/run/nginx/log-peek", "socket for local connections")
)

// Read from a named pipe (FIFO), re-opening the file whenever reading
// returns EPIPE (i.e., the pipe is closed from the other side).
type pipeReader struct {
	path   string
	reader *os.File
}

func NewPipeReader(path string) (*pipeReader, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	return &pipeReader{
		path:   path,
		reader: f,
	}, nil
}

func (r *pipeReader) Close() error {
	return r.reader.Close()
}

func (r *pipeReader) Read(buf []byte) (int, error) {
	for {
		n, err := r.reader.Read(buf)
		if err != nil {
			if err == io.ErrClosedPipe || err == io.EOF {
				if r.reader, err = os.Open(r.path); err == nil {
					continue
				}
			}
			return 0, err
		}
		return n, nil
	}
}

// Server listens on a UNIX socket and multiplexes data read from a
// pipeReader onto eventual connections. Data is consumed from the
// pipeReader even if there are no connections. It is assumed that
// clients can consume input as quickly as it is written, otherwise
// the whole Server will block waiting for them (which isn't a
// particularly clever design, but it works for the intended use
// case).
type Server struct {
	lock     sync.Mutex
	pipe     *pipeReader
	conns    map[net.Conn]struct{}
	listener *net.UnixListener
}

func NewServer(pipe *pipeReader, socketPath string) (*Server, error) {
	os.Remove(socketPath)

	l, err := net.ListenUnix("unix", &net.UnixAddr{socketPath, "unix"})
	if err != nil {
		return nil, err
	}
	return &Server{
		pipe:     pipe,
		listener: l,
		conns:    make(map[net.Conn]struct{}),
	}, nil
}

func (s *Server) readLogs() {
	buf := make([]byte, 65536)
	for {
		n, err := s.pipe.Read(buf)
		if err != nil {
			log.Fatal(err)
		}

		data := buf[:n]
		s.lock.Lock()
		for conn := range s.conns {
			_, err := conn.Write(data)
			if err != nil {
				delete(s.conns, conn)
				conn.Close()
			}
		}
		s.lock.Unlock()
	}
}

func (s *Server) Run() {
	go s.readLogs()

	for {
		conn, err := s.listener.AcceptUnix()
		if err != nil {
			log.Fatal(err)
		}
		s.lock.Lock()
		s.conns[conn] = struct{}{}
		s.lock.Unlock()
	}
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	pipe, err := NewPipeReader(*logFifoPath)
	if err != nil {
		log.Fatal(err)
	}
	srv, err := NewServer(pipe, *socketPath)
	if err != nil {
		log.Fatal(err)
	}
	srv.Run()
}
